'use strict'

exports.handler = function(event, context, callback) {
  var response = {
    statusCode: 200,
    headers: {
      'Content-Type': 'text/html; charset=utf-8'
    },
    body: '<p>Hello IDP Folks! This is Lambda function using terraform</p>'
  }
  callback(null, response)
}
